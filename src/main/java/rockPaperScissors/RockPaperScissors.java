package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    private String computerMove;
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        // rundenummer
        System.out.println("Let's play round " + roundCounter);

        // maskin velger tilfeldig rps
        Random r = new Random();
        computerMove = rpsChoices. get(r.nextInt(rpsChoices.size()));

        // Tar inn bruker
        String userMove;

            System.out.println("Your choice (Rock/Paper/Scissors)?");
            userMove = sc.nextLine();
            userMove = userMove.toLowerCase();

            if(rpsChoices.contains(userMove)){
                winner(userMove, computerMove);
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                System.out.println("Do you wish to continue playing? (y/n)?");
                String answer = sc.nextLine();
                if (answer.equals("y")){
                    roundCounter ++;
                run();
                }
                else{
                    System.out.println("Bye bye :)");
                }
            }

            else {
                System.out.println("I do not understand " + userMove + ".Could you try again?");
                run();
            }
         }

   
              
   
    public void winner(String userMove, String computerMove){
        // lager tekst som strings, slik at jeg slipper å skrive alle inn flere ganger
        String hC = "Human chose ";
        String cH = ", computer chose ";
        String tie = ". It's a tie!";
        String hW = ". Human wins!";
        String cW = ". Computer wins!";
        //hvem vinner
        if (userMove.equals(computerMove)){
            System.out.println(hC + userMove + cH + computerMove + tie);
        } else if (userMove.equals("rock") && computerMove.equals("scissors")){
            System.out.println(hC + userMove + cH + computerMove + hW);
            humanScore ++;
        } else if (userMove.equals("paper") && computerMove.equals("rock")){
            System.out.println(hC + userMove + cH + computerMove + hW);
            humanScore ++;
        } else if (userMove.equals("scissors") && computerMove.equals("paper")){
            System.out.println(hC + userMove + cH + computerMove + hW);
            humanScore ++;
        } else {
            System.out.println(hC + userMove + cH + computerMove + cW);
            computerScore ++;
        }

    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
